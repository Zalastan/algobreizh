<?php
session_start();
include_once ("../content/include.html");
header('Content-type:text/html; charset=utf-8');
if (isset($_SESSION['id']))
{
	if(isset($_SESSION['statut'])==2)
	{
		// importation des données d'accès à la base de donnée

		// importation de la méthode d'accès à la base de donnée
		require_once '../models/connexion/connectdb.php';
		// importation des modèles de données à traiter
		require_once '../models/visiteur/functions.php';

		/*On charge la liste d'ENUM de la colonne 'nom' de la table TypesFrais*/
		
		$liste_nom_type_frais = liste_frais();
		$periode = date("01-m-Y");
		$periode_frais = date_time($periode);
		$controlErreur = 0;
		/*traitement de l'envoi des frais*/
			/*traitement de l'envoi des frais*/
		if (isset ( $_POST ['envoi_frais'] )) 
		{	
			$pattern_montant ="#[0-9]#";
			$pattern_date = "#(((0[1-9]|[12][0-9]|3[01])([-])(0[13578]|10|12)([-])(\d{4}))|(([0][1-9]|[12][0-9]|30)([-])(0[469]|11)([-])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([-])(02)([-])(\d{4}))|((29)(\.|-|\/)(02)([-])([02468][048]00))|((29)([-])(02)([-])([13579][26]00))|((29)([-])(02)([-])([0-9][0-9][0][48]))|((29)([-])(02)([-])([0-9][0-9][2468][048]))|((29)([-])(02)([-])([0-9][0-9][13579][26])))#";
			$pattern_general = "#^[0-9-a-zA-Zàâäéèêëïîôöùûüç ]+$#";
			if  (!preg_match($pattern_general, $_POST['libelle_frais']))
			{
				$controlErreur = 1;
				echo"<script>
				 		$(document).ready(function(){
								$( '#msgErreur' ).append( 'Vous devez renseigner un libellé valide !</br>' );
     								return false;
				 		});
                   </script>";					
			} 
			elseif(!preg_match($pattern_montant, $_POST['montant_forfait']))
			{
				$controlErreur = 1;
				echo"<script>
				 		$(document).ready(function(){
								$( '#msgErreur' ).append( 'Vous devez renseigner un montant valide !</br>' );
     								return false;
				 		});
                    </script>";								
			}
			elseif(!preg_match($pattern_date, $_POST['date_frais']))
			{
				$controlErreur = 1;
				echo"<script>
				 		$(document).ready(function(){
								$( '#msgErreur' ).append( 'Vous devez renseigner une date valide !</br>' );
     								return false;
				 		});
                    </script>";				
			}

			else
			{	
				$id_frais = getLastIdFrais();
				$id_utilisateur = $_SESSION['id'];
				$type_frais = htmlspecialchars ( $_POST ['type_frais'] );
				$id_typeFrais = get_id_type_frais($type_frais);
				$libelle = htmlspecialchars ( $_POST ['libelle_frais'] );
				$montantReel = htmlspecialchars ( $_POST ['montant_forfait'] );
				$date_fr = htmlspecialchars ( $_POST ['date_frais'] );
				$statut="En cours de traitement";
				$date_frais = date_time($date_fr);
				//Créer un dossier justificatifs
				ajout_frais($id_typeFrais, $id_utilisateur, $libelle, $montantReel, $date_frais, $periode_frais, $statut);
                
               	if ($_FILES['justificatif']['name']!="") {
	                mkdir('../justificatifs/'.$_SESSION['id'].'/'.$periode_frais.'/', 0777, true);
					$dossier = '../justificatifs/'.$_SESSION['id'].'/'.$periode_frais.'/';
					$fichier = basename($_FILES['justificatif']['name']);
					$taille_maxi = 1000000;
					$taille = filesize($_FILES['justificatif']['tmp_name']);
					$extensions = array('.png', '.gif', '.jpg', '.jpeg', '.pdf');
					$extension = strrchr($_FILES['justificatif']['name'], '.');
					$chemin = $dossier;
					$nom = $fichier;
					ajout_justificatif((int)$id_frais, $fichier, $dossier);
					//Début des vérifications de sécurité...
					if(!in_array($extension, $extensions)) //Si l'extension n'est pas dans le tableau
					{
						$controlErreur = 1;
	                    echo"<script>
				 		$(document).ready(function(){
								$( '#msgErreur' ).append( 'Vous devez uploader un fichier de type png, gif, jpg, jpeg, txt ou doc...</br>' );
     								return false;
				 		});
                    </script>";	
					}
					if($taille>$taille_maxi)
					{
					    $controlErreur = 1;
	                    echo"<script>
				 		$(document).ready(function(){
								$( '#msgErreur' ).append( 'Le fichier est trop volumineux</br>' );
     								return false;
				 		});
                    </script>";
					}
					if(!isset($erreur)) //S'il n'y a pas d'erreur, on upload
					{
					     //On formate le nom du fichier ici...
					     $fichier = strtr($fichier, 
					          'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 
					          'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
					     $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);
					     if(move_uploaded_file($_FILES['justificatif']['tmp_name'], $dossier . $fichier)) //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
					     {
					          echo"<script>
					 			$(document).ready(function(){
					 			      $('#msgOk').toggle(400);
	     								return false;
					 			});
	                    		</script>";
					     }
					     else //Sinon (la fonction renvoie FALSE).
					     {
					          echo"<script>
							$(document).ready(function(){
								$('#msgErreur').toggle(400);
								return false;
							});
	                    	</script>";
					     }
					}
					else
					{
					 echo"<script>
					 		$(document).ready(function(){
					 			      $('#msgOk').toggle(400);
	     								return false;
					 		});
	                    </script>";
					}
					if($controlErreur == 1){
						echo"<script>
								$(document).ready(function(){
									$('#msgErreur').toggle(400);
									return false;
								});
		                    </script>";
					}
				}
			echo"<script>
					 		$(document).ready(function(){
					 			      $('#msgOk').toggle(400);
	     								return false;
					 		});
	                    </script>";
			}
		}
		//On charge la vue en dernier
		require_once '../views/visiteur/gestion_frais.php';
		
	}
	else
		echo'<script>alert("Vous n\'êtes pas autorisé à vous connecter sur cette page !");
	window.location.replace("/algobreizh");
</script>';
}
else
	echo'<script>alert("Veuillez vous identifier !");
window.location.replace("/algobreizh");
</script>';
?>