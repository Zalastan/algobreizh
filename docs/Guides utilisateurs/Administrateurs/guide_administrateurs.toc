\select@language {french}
\contentsline {section}{\numberline {1}Connexion \IeC {\`a} l'application AlgoBreizh}{1}
\contentsline {paragraph}{L'administrateur pourra se connecter \IeC {\`a} la base de donn\IeC {\'e}es avec l'identifiant et le mot de passe qui lui ont \IeC {\'e}t\IeC {\'e} attribu\IeC {\'e}s.}{1}
\contentsline {section}{\numberline {2}Ajout d'un utilisateur}{1}
\contentsline {paragraph}{Pour ajouter un utilisateur l'administrateur devra cliquer sur le lien \"ajouter un utilisateur\".}{1}
\contentsline {section}{\numberline {3}Supprimer un utilisateur}{1}
