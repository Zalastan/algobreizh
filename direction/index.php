<?php
/* CONTROLLEUR DE LA PAGE INDEX des administrateur */

session_start();
header('Content-type:text/html; charset=utf-8');

if (isset($_SESSION['id'])) {
	if (isset($_SESSION['statut'])) {
		require_once '../views/direction/index.php';
	}	
} else {
	echo '<script>alert("Veuillez vous identifier !");window.location.replace("/");</script>';
}
