<?php
/** Cette page ferme la session et redirige vers la page de connexion
 *
*/
header('Content-type:text/html; charset=utf-8');
session_start();
// Suppression des variables de session et de la session
$_SESSION = array();
session_unset();
session_destroy();

header("Location:/");
?>