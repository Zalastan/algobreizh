<?php
session_start();
header('Content-type:text/html; charset=utf-8');

if (isset($_SESSION['id']))
{
require_once '../models/connexion/connectdb.php';
require_once '../models/session/functions.php';	
/*charger les information utilisateurs*/
$get_info = get_info(($_SESSION['id']));
$username=$get_info['username'];
$nom=$get_info['nom'];
$prenom=$get_info['prenom'];
$adresse1=$get_info['adresse1'];
$adresse2=$get_info['adresse2'];
$email=$get_info['email'];
$ville=$get_info['ville'];
$pays=$get_info['pays'];
$telephonePortable=$get_info['telephonePortable'];
$telephoneFixe=$get_info['telephoneFixe'];
$codePostal=$get_info['codePostal'];
$dateInscription=$get_info['dateInscription'];
$profession = get_statut($_SESSION['statut']);

require_once '../views/session/profil.php'; 
// importation des données d'accès à la base de donnée
}
else {
	echo'<script>alert("Veuillez vous identifier !");
	window.location.replace("/algobreizh");
					</script>';
}

?>