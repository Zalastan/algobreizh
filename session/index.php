<?php
session_start();
header('Content-type:text/html; charset=utf-8');
// importation des données d'accès à la base de donnée
require_once '../models/connexion/connectdb.php';
require_once '../models/session/functions.php';


if (isset($_POST['username']) && isset($_POST['password'])) {
	if (!empty($_POST['username']) && !empty($_POST['password'])) {
		$username = htmlspecialchars($_POST['username']);
		$password = htmlspecialchars($_POST['password']);
		$result = est_present($username);
		$id = $result['id'];
		$statut = $result['statut'];
		$hash = password_hash($password, PASSWORD_BCRYPT, array("cost" => 10));  // on hache le mot de passe	
		$password_db = get_password($id);

		if (password_verify($password, $password_db) && $id) {
			$_SESSION['id'] = $id;
			$_SESSION['username'] = $username;
			$_SESSION['statut'] = $statut;
			if ($statut == 1) {
				echo '<script>window.location.replace("/admin/index.php");</script>';
			} elseif ($statut == 2) {
				echo '<script>window.location.replace("/visiteur/visiteur.php");</script>';
			} elseif ($statut == 3) {
				echo '<script>window.location.replace("/comptable/validation_frais.php");</script>';
			} else {
				echo '<script>window.location.replace("/direction/index.php");</script>';
			}
		} else {
			echo "<script> alert('Vous devez renseigner un username et un mot de passe valide !');  
	         					window.location.replace('/');
				</script>";
		}
	} else {
		echo "<script> alert('Vous devez remplir tous les champs !');
						window.location.replace('/session/index.php');
					</script>";
	}
}
