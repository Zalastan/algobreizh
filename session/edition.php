<?php
session_start ();
header ( 'Content-type:text/html; charset=utf-8' );

if (isset ( $_SESSION ['id'] )) {
	require_once '../models/connexion/connectdb.php';
	require_once '../models/session/functions.php';
	require_once '../views/session/profil.php';
	
	if (isset ( $_POST ['edition'] )) {
		$id = $_SESSION ['id'];
		$pattern_email = "#^(\w[-._+\w]*\w@\w[-._\w]*\w\.\w{2,3})$#";
		$pattern_telephone = "#^((\+|00)33\s?|0)[1-9](\s?\d{2}){4}$#";
		$pattern_general = "#^[0-9-a-zA-Zàâäéèêëïîôöùûüç ]+$#";
		if (! empty ( $_POST ['adresse1'] ) && ! empty ( $_POST ['codePostal'] ) && ! empty ( $_POST ['ville'] ) && ! empty ( $_POST ['pays'] ) && ! empty ( $_POST ['telephonePortable'] ) && (preg_match ( $pattern_general, $_POST ['pays'] )) && (preg_match ( $pattern_general, $_POST ['adresse1'] )) && (preg_match ( $pattern_general, $_POST ['codePostal'] )) && (preg_match ( $pattern_general, $_POST ['ville'] )) && (preg_match ( $pattern_telephone, $_POST ['telephonePortable'] ))) {
			$adresse1 = htmlspecialchars ( $_POST ['adresse1'] );
			$adresse2 = htmlspecialchars ( $_POST ['adresse2'] );
			$pays = htmlspecialchars ( $_POST ['pays'] );
			$codePostal = htmlspecialchars ( $_POST ['codePostal'] );
			$ville = htmlspecialchars ( $_POST ['ville'] );
			$telephoneFixe = htmlspecialchars ( $_POST ['telephoneFixe'] );
			$telephonePortable = htmlspecialchars ( $_POST ['telephonePortable'] );
		}
		if (isset ( $_POST ['pays'] ) && (! preg_match ( $pattern_general, $_POST ['pays'] ))) {
			echo '
								<script> alert("Vous devez renseigner un pays valide !");  
	         					window.location.replace("../session/profil.php");
								</script>';
		} elseif (isset ( $_POST ['adresse1'] ) && (! preg_match ( $pattern_general, $_POST ['adresse1'] ))) {
			echo '
								<script> alert("Vous devez renseigner une adresse valide !");  
	         					window.location.replace("../session/profil.php");
								</script>';
		} elseif (! empty ( $_POST ['adresse2'] ) && (! preg_match ( $pattern_general, $_POST ['adresse2'] ))) {
			echo '
								<script> alert("Vous devez renseigner une adresse2 valide !");  
	         					window.location.replace("../session/profil.php");
								</script>';
		} 

		elseif (isset ( $_POST ['codePostal'] ) && (! preg_match ( $pattern_general, $_POST ['codePostal'] ))) {
			echo '
									<script> alert("Vous devez renseigner un code postal valide !");  
	         					window.location.replace("../session/profil.php");
									</script>';
		} 

		elseif (isset ( $_POST ['ville'] ) && (! preg_match ( $pattern_general, $_POST ['ville'] ))) {
			echo '
								<script> alert("Vous devez renseigner une ville valide !");  
	         					window.location.replace("../session/profil.php");
								</script>';
		} elseif (! empty ( $_POST ['telephoneFixe'] ) && isset ( $_POST ['telephoneFixe'] ) && (! preg_match ( $pattern_telephone, $_POST ['telephoneFixe'] ) && ! preg_match ( "Téléphone Fixe (faculatif)", $_POST ['telephoneFixe'] ))) {
			echo '
								<script> alert("Vous devez renseigner un numéro de téléphone valide !");  
	         					window.location.replace("../session/profil.php");
								</script>';
		} 

		elseif (isset ( $_POST ['telephonePortable'] ) && (! preg_match ( $pattern_telephone, $_POST ['telephonePortable'] ))) {
			echo '
								<script> alert("Vous devez renseigner un numéro de téléphone valide !");  
	         					window.location.replace("../session/profil.php");
								</script>';
		} else {
			editUtilisateur ( $id, $adresse1, $adresse2, $codePostal, $ville, $pays, $telephoneFixe, $telephonePortable );
			echo '<script> alert("Modification de profil prise en compte !");  
	         					window.location.replace("../session/profil.php");
								</script>';
		}
	}
	
	// Gestion de la modification des mots de passe //
	if (isset ( $_POST ['editpassword'] )) {
		$pattern_password = "#^((?=.*\d)(?=.*[a-zA-Z])[a-zA-Z0-9!@#$%&*]{6,20})$#";
		if (! empty ( $_POST ['password'] )) {
			if (! empty ( $_POST ['password1'] ) && (! preg_match ( $pattern_password, $_POST ['password1'] ))) {
				if (! empty ( $_POST ['password2'] ) && (! preg_match ( $pattern_password, $_POST ['password2'] ))) {
					$password = htmlspecialchars ( $_POST ['password'] );
					$password1 = htmlspecialchars ( $_POST ['password1'] );
					$password2 = htmlspecialchars ( $_POST ['password2'] );
					$password_db = get_password ( $_SESSION ['id'] );
					if (password_verify ( $password, $password_db )) {
						if ($password1 == $password2) {
							edit_password ( $_SESSION ['id'], password_hash ( $password2, PASSWORD_BCRYPT, array (
									"cost" => 10 
							) ) );
							echo '<script> alert("La modification du mot de passe a été prise en compte !");  
	         					window.location.replace("../session/profil.php");
								</script>';
						} else {
							echo '
						<script> alert("Vos mots de passe ne correspondent pas !");
						window.location.replace(../session/profil.php");

						</script>';
						}
					} else {
						echo '
				<script> alert("Votre mot de passe actuel est éronné !");
				window.location.replace("../session/profil.php");

				</script>';
					}
				} else {
					echo '
			<script> alert("Vous ne respectez pas la conformité du mot de passe !");
			window.location.replace("../session/profil.php");

			</script>';
				}
			} else {
				echo '
			<script> alert("Vous ne respectez pas la conformité du mot de passe !");
			window.location.replace("../session/profil.php");
			</script>';
			}
		} else {
			echo '
			<script> alert("Vous devez entrer votre mot de passe actuel !");
			window.location.replace("../session/profil.php");
			</script>';
		}
	}
} else
	echo '<script>alert("Veuillez vous identifier !");
	window.location.replace("../session/profil.php");
					</script>';
?>