<?php
/* CONTROLLEUR DE LA PAGE COMPTABLE validation_frais */
session_start();
header('Content-type:text/html; charset=utf-8');

/*Si la session est bien ouverte*/
if (isset($_SESSION['id'])) {
	/*Si le visiteur est un comptable on active le modèle et on affiche la vue*/
	if (isset($_SESSION['statut']) == 3 || (isset($_SESSION['statut']) == 4)) {
		require_once '../models/connexion/connectdb.php';
		require_once '../models/comptable/functions.php';
		if (isset($_POST['id_utilisateur'])) {
			$id = $_POST['id_utilisateur'];
		}
		if (isset($_POST['periode'])) {
			$periode = $_POST['periode'];
		}
		$liste_utilisateurs = get_list_utilisateurs();
		if (isset($id) && isset($periode)) {
			$liste_des_frais = get_frais_comptable($periode, $id);
		}
		//chargement de la vue
		if (isset($_POST['valider_edit_frais'])) {
			$id_frais = $_POST['id_frais'];
			$libelle = $_POST['libelle'];
			$statut_frais = $_POST['edited_statut'];

			editfrais($id_frais, $libelle, $statut_frais);
		}
		require_once '../views/comptable/validation_frais.php';
	} else // Le visiteur est redirigé vers la page d'accueil.
		echo '<script>alert("Vous n\'êtes pas autorisé à vous connecter sur cette page !");
	window.location.replace("/.php");
					</script>';
} else
	echo '<script>alert("Veuillez vous identifier !");
	window.location.replace("/");
					</script>';
