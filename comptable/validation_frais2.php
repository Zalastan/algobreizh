<?php
require_once '../models/comptable/functions.php';

if (isset($_POST['dataSend'])) {
	//réception des données
	$dataSend = $_POST['dataSend'];
	$params = array();
	parse_str($dataSend, $params);


	// TRAITEMENT
	$liste_periodes = liste_periodes((int)$params['id_utilisateur']);
	$periodes = [];
	if ($params['id_utilisateur'] != -1) {
		foreach ($liste_periodes as $data) {
			$periodes[] = $data['periode_frais'];
		}
	}
	
	$data = [];
	$data["id_utilisateur"] = $params['id_utilisateur'];

	$data["test"] = $params;

	// 	if(empty($_POST['periode'])){
	// 		if(isset($periodes)){
	// 			$data["periode"] = $periodes[0];
	// 		}elseif(isset($_POST['periode'])){
	// 			$data["periode"] = $_POST['periode'];
	// 		}else{
	// 			$data["periode"] = null;
	// 		}
	// 	}	

	// 	if(isset($data["periode"])){
	// 		$liste_des_frais = get_frais_comptable($data["periode"], $data["id_utilisateur"]);
	// 		if(isset($liste_des_frais)){
	// 			while ($donnees = $liste_des_frais->fetch()) {
	// 				$frais[] = array(
	// 						"idFrais" =>$donnees["idFrais"],
	// 						"type_de_frais" => $donnees["type de frais"],
	// 						"libelle" => $donnees["libelle"],
	// 						"montantReel" => $donnees["montantReel"],
	// 						"date_frais" => $donnees["date_frais"],
	// 						"statut" => $donnees['statut'],
	// 						"portable" => $donnees['telephonePortable']
	// 				);
	// 			}
	// 		}
	// 	}

	// 	if(isset($frais)){
	// 		$data["frais"] = $frais;
	// 	}



	if (empty($_POST['periode'])) {
		if (isset($periodes)) {
			$data["periode"] = $periodes[0];

			
			// 2012-01-01
			$liste_des_frais = get_frais_comptable($data["periode"], (int) $data["id_utilisateur"]);
			
			if (isset($liste_des_frais)) {
				while ($donnees = $liste_des_frais->fetch()) {
					$frais[] = array(
						"idFrais" => $donnees["idFrais"],
						"type_de_frais" => $donnees["type de frais"],
						"libelle" => $donnees["libelle"],
						"montantReel" => $donnees["montantReel"],
						"date_frais" => $donnees["date_frais"],
						"statut" => $donnees['statut'],
						"portable" => $donnees['telephonePortable']
					);
				}
				if (isset($frais)) {
					$data["frais"] = $frais;
				}
			}
		} else {
			$data["periode"] = null;
		}
	} else {
		$data["periode"] = $_POST['periode'];
		$liste_des_frais = get_frais_comptable($data["periode"], $data["id_utilisateur"]);
		if (isset($liste_des_frais)) {
			while ($donnees = $liste_des_frais->fetch()) {
				$frais[] = array(
					"idFrais" => $donnees["idFrais"],
					"type_de_frais" => $donnees["type de frais"],
					"libelle" => $donnees["libelle"],
					"montantReel" => $donnees["montantReel"],
					"date_frais" => $donnees["date_frais"],
					"statut" => $donnees['statut'],
					"portable" => $donnees['telephonePortable']
				);
			}
			if (isset($frais)) {
				$data["frais"] = $frais;
			} else {
				$data["periode"] = null;
			}
		}
	}

	if (isset($periodes)) {
		$data["periodes"] = $periodes;
	}


	// =======ENVOIE=================================================================================================================================================================
	//===============================================================================================================================================================================
	if ($params['id_utilisateur'] != -1) {
		header('Content-Type: application/json');
		echo json_encode($data);
	} else {
		echo "Aucun utilisateur sélectionné";
	}
}
