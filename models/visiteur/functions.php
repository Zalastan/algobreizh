
<?php
/**
* Fonction retournant un tableau contenant sous forme de liste, les noms des différents types de frais
*/
function liste_frais()
{
$bdd=connectdb(); // connection à la base de donnée
$sql_liste_type = "SELECT nom FROM TypesFrais";
$reponse = $bdd->prepare($sql_liste_type);
$reponse->execute();

return $reponse;
}

 /** Cette fonction retourne l'id du nom du type de frais placé en paramètre ex : Hebergement retourne un entier (son id)
  *
  */
function get_id_type_frais($type_frais){
$bdd=connectdb(); // connection à la base de donnée
$req = $bdd->prepare("SELECT id_typeFrais FROM TypesFrais WHERE nom LIKE '$type_frais'");
$req->execute(array(
        'type_frais' => $type_frais));
    $resultat = $req->fetch();
    $resultat = array(
        'id_typeFrais' => $resultat['id_typeFrais']); 
    return $resultat['id_typeFrais'];   
}

/** Cette requete se contente d'entrer un frais dans la table des frais
  *
  */
function ajout_frais($id_typeFrais, $id_utilisateur, $libelle, $montantReel, $date_frais, $periode_frais, $statut)
 {
 $bdd=connectdb(); // connection à la base de donnée
 try
 {
 

  $insertFrais = "INSERT INTO Frais(id_typeFrais,id_utilisateur,libelle, montantReel,date_frais, periode_frais, statut)
      VALUES( 
                          '$id_typeFrais',
                          '$id_utilisateur',
                          '$libelle',
                          '$montantReel',
                          '$date_frais',
                          '$periode_frais',
                          '$statut'
                  
                  )
            ";
      $req = $bdd->prepare($insertFrais);
      $req->execute(array(
        'id_typeFrais' => $id_typeFrais,
        'id_utilisateur' => $id_utilisateur,
        'libelle' => $libelle,
        'montantReel'=> $montantReel,
        'date_frais' => $date_frais,
        'periode_frais' => $periode_frais,
        'statut' => $statut));
      $req-> fetch();
      }
      catch(Exception $e)
      {
      	$_SESSION['erreur'] = 1;
      }
}


/** Cette requete transforme une date française en date US pour stockage dans la bdd
 *
*/
function date_time($date_fr) {
  return strftime('%Y-%m-%d',strtotime($date_fr));
}

/** Cette requete affiche un message encadré vert confirmant la prise en compte des frais
 *
*/
function success() {
      echo'<div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            Vous venez de téléverser un nouveau frais.</div>';
    }

 /** Cette fonction retourne la liste des frais d'un utilisateur d'une période précise (un mois précis)
 * 
 */
function get_frais($periode_frais, $idUtilisateurs) {
$bdd=connectdb(); // connection à la base de donnée
    $req = $bdd->query("SELECT tf.nom, f.libelle, f.montantReel, f.date_frais, f.statut
FROM Frais f, TypesFrais tf
WHERE f.id_typeFrais = tf.id_typeFrais
AND f.id_utilisateur =$idUtilisateurs
AND f.periode_frais = '$periode_frais'");
   $resultat = $req;
    return $resultat; 
}

/** Cette requete liste les prériodes pour lesquelles l'utilisateur (id) à déposé des frais
 *
*/
function liste_periode($intervalle, $id_utilisateur)
{
  $bdd=connectdb(); // connection à la base de donnée
    $req = $bdd->query("SELECT DISTINCT periode_frais FROM Frais WHERE periode_frais > DATE_SUB(CURDATE(), interval '$intervalle' YEAR)
                          AND id_utilisateur = $id_utilisateur");
     $resultat = $req;
    return $resultat; 
}

function getLastIdFrais(){
	$bdd=connectdb(); // connection à la base de donnée

	$req = $bdd->query("SELECT MAX(idFrais) FROM Frais");
	$last_id = $req->fetch();
	$last_id=$last_id['0'];
	return $last_id+1;
}

function ajout_justificatif($id_frais, $nom, $chemin){
	$bdd=connectdb(); // connection à la base de donnée
	$insertJustificatif = "INSERT INTO Justificatifs (id_frais,nom,chemin)
	VALUES('$id_frais','$nom','$chemin')";
	$req = $bdd->prepare($insertJustificatif);
	$req->execute(array(
			'id_frais' => $id_frais,
			'nom' => $nom,
			'chemin' => $chemin));
	$req-> fetch();
}

?>