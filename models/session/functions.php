<?php

/** Cette fonction retourne dans un array l'id et le statut auquel correspond l'utilisateur si il 
 * s'il est présent dans la bdd
 */
function est_present($username)
{
  $bdd = connectdb();
  $req = $bdd->prepare('SELECT idUtilisateurs, statut FROM Utilisateurs WHERE username = :username');
  $req->execute(array(
    'username' => $username
  ));
  $resultat = $req->fetch();
  $resultat = array(
    'id' => $resultat['idUtilisateurs'],  
    'statut' => $resultat['statut']
  );
  return $resultat;
}
/** Cette fonction retourne un array des donéees de l'utilisateur dont l'id correspond à la variable d'entrée
 *
 */
function get_info($idUtilisateurs)
{
  $bdd = connectdb();
  $req = $bdd->prepare("SELECT username, nom, prenom, 
			adresse1, adresse2, ville, codePostal, telephoneFixe, telephonePortable, pays,
			email, dateInscription 
			FROM Utilisateurs 
			WHERE idUtilisateurs = :idUtilisateurs");
  $req->execute(array(
    'idUtilisateurs' => $idUtilisateurs
  ));

  $resultat = $req->fetch();
  return $resultat;
}

/** get_statut : Transforme l'idStatut en entrée par son appelation en claire
 *
 */
function get_statut($statut)
{
  if ($statut == 1) {
    return "Administrateur";
  } elseif ($statut == 2) {
    return "Commercial";
  } else
    return "Comptable";
}

/** editUtilisateur : cette requete modifie les valeurs stockée dans la table de l'utilisateur
 *
 */
function editUtilisateur(
  $id,
  $adresse1,
  $adresse2,
  $codePostal,
  $ville,
  $pays,
  $telephoneFixe,
  $telephonePortable
) {
  $bdd = connectdb(); // connection à la base de donnée
  $editUtilisateurs = " UPDATE Utilisateurs 
  						SET
                adresse1 = :adresse1,
                    adresse2 = :adresse2,
                    codePostal = :codePostal,
                    ville = :ville,
                    pays = :pays,
                    telephoneFixe = :telephoneFixe,
                    telephonePortable = :telephonePortable
              WHERE idUtilisateurs = :id";
  $req = $bdd->prepare($editUtilisateurs);
  $req->execute(array(
    'id' => $id,
    'adresse1' => $adresse1,
    'adresse2' => $adresse2,
    'codePostal' => $codePostal,
    'ville' => $ville,
    'telephoneFixe' => $telephoneFixe,
    'telephonePortable' => $telephonePortable,
    'pays' => $pays
  ));
  $req->fetch();
}

/** Cette fonction retourne le mot de passe de l'utilisateur dont l'id est en entrée
 *
 */
function get_password($idUtilisateurs)
{
  $bdd = connectdb(); // connection à la base de donnée
  $req = $bdd->prepare("SELECT password FROM Utilisateurs WHERE idUtilisateurs = :idUtilisateurs");
  $req->execute(array(
    'idUtilisateurs' => $idUtilisateurs
  ));
  $resultat = $req->fetch();
  $resultat = array(
    'password' => $resultat['password']
  );
  return $resultat['password'];
}

/** Cette fonction change le mot de passe de l'utilisateur en paramètre (son id).
 *
 */
function edit_password($idUtilisateurs, $newpassword)
{

  $bdd = connectdb(); // connection à la base de donnée
  $edit_password = " UPDATE Utilisateurs 
  						SET
                            password = '$newpassword'
                  		WHERE idUtilisateurs = '$idUtilisateurs'
            ";
  $req = $bdd->prepare($edit_password);
  $req->execute(array(
    'password' => $newpassword
  ));
  $req->fetch();
}
