<?php
header('Content-type:text/html; charset=utf-8');
if (isset($_SESSION['id']))
{
    if(isset($_SESSION['statut']))
    {
        ?>

<!DOCTYPE html>
<html lang="fr">

<head>
<meta charset="utf-8">
<title>Gestion administrateur</title>
<meta name="Author" lang="fr" content="GAMARDE Sébastien & SAMSON Denis & PLAISIER Sylvain"> 
<meta name="description" content="Appli Frais Algobreizh" />
<meta name="robots" content="noindex, nofollow, noarchive" />
	<!--  Bootstrap Style -->
    <link href="../assets/css/bootstrap.css" rel="stylesheet" />
    <!--  Font-Awesome Style -->
    <link href="../assets/css/font-awesome.min.css" rel="stylesheet" />
    <!--  Animation Style -->
    <!--  Google Font Style -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!--  Custom Style -->
    <link href="../assets/css/style.css" rel="stylesheet" />
</head>

<body>	
		<?php include '../content/nav.php';?>
		<div id="admin" class="pad-top-botm" >
        <div class="container">
            <div class="row text-center ">
                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                    <h2 data-wow-delay="0.3s" class="wow rollIn animated"><strong>Gestion administrateur </strong></h2>
                    <p class="sub-head">Gérer les inscriptions et suppression de compte en toute sérénité.</p>
                    
                </div>
            </div>
            <div class="row ">
                <div class="col-lg-4 col-lg-offset-2 col-md-4  col-md-offset-2 col-sm-4 col-sm-offset-2  col-xs-6 ">

                    <div class="portfolio-item wow rotateIn animated" data-wow-delay="0.4s">


                        <img src="../assets/img/ajouter_compte.png" class="img-responsive " alt="" />
                        <div class="overlay">
                            <p><a href = "../admin/inscription.php">Ajouter un compte</a></p>
                        </div>
                    </div>
                </div>
                 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">

                    <div class="portfolio-item wow rotateIn animated" data-wow-delay="0.4s">


                        <img src="../assets/img/supprimer_compte.png" class="img-responsive " alt="" />
                        <div class="overlay">
                            <p><a href = "../admin/suppression.php">Supprimer un compte</a></p>
                        </div>
                    </div>
                </div>
            </div>
           

        </div>
        </div>
         <script src="../assets/js/jquery-1.10.2.js"></script>
    		<!--  Core Bootstrap Script -->
   			 <script src="../assets/js/bootstrap.js"></script>
</body>
</html>
<?php
}
    else
    echo'<script>alert("Vous n\'êtes pas autorisé à vous connecter sur cette page !");
    window.location.replace("/algobreizh");
                    </script>';
}
else
echo'<script>alert("Veuillez vous identifier !");
    window.location.replace("/algobreizh");
                    </script>';
?>