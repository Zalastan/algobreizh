<?php
header('Content-type:text/html; charset=utf-8');
if (isset($_SESSION['id'])) {
	if (isset($_SESSION['statut']) == 1) {
?>
		<!DOCTYPE html>
		<html lang="fr">

		<head>
			<meta charset="utf-8">
			<title>Inscription</title>
			<meta name="Author" lang="fr" content="GAMARDE Sébastien & SAMSON Denis & PLAISIER Sylvain">
			<meta name="description" content="Appli Frais Algobreizh" />
			<meta name="robots" content="noindex, nofollow, noarchive" />
			<meta name="robots" content="noindex, nofollow, noarchive" />

			<link href="../assets/css/bootstrap.css" rel="stylesheet" />
			<!--  Font-Awesome Style -->
			<link href="../assets/css/font-awesome.min.css" rel="stylesheet" />
			<!--  Animation Style -->
			<!--  Google Font Style -->
			<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
			<!--  Custom Style -->
			<link href="../assets/css/style.css" rel="stylesheet" />
		</head>

		<body>
			<?php include '../content/nav.php'; ?>
			<div class="row pad-top-botm">
				<div class="row text-center ">
					<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
						<h2 data-wow-delay="0.3s" class="wow rollIn animated"><strong>Suppression d'utilisateurs</strong></h2>
						<p class="sub-head">Supprimer définivement un utilisateur.</p>

					</div>
				</div>

				<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
					<h1 class="text-center">Liste des membres</h1>
					<table class="table table-responsive text-center">
						<tr>
							<th>ID</th>
							<th>Nom d'utilisateur</th>
							<th>Prénom</th>
							<th>Nom</th>
							<th>Supprimer</th>
						</tr>
						<?php
						// On affiche chaque entrée une à une
						$bdd = connectdb();
						$reponse = $bdd->prepare('SELECT * FROM Utilisateurs');
						$reponse->execute();
						$donnees = $reponse->fetchAll();
						
						foreach ($donnees as $donnee) {
						?><tr>
								<td> <?php echo $donnee['idUtilisateurs']; ?></td>
								<td> <?php echo $donnee['username']; ?></td>
								<td> <?php echo $donnee['prenom']; ?></td>
								<td> <?php echo $donnee['nom']; ?></td>
								<td>
									<form name="form-suppression" method="post" action="../admin/suppression.php">
										<button type="submit" value="<?php echo $donnee['idUtilisateurs'] ?>" name="suppression" class="btn btn-danger">
											<i class="fa fa-trash"></i> Supprimer le compte</button>
									</form>
								</td>
							</tr>


						<?php } ?>
					</table>
				</div>
			</div>

			<script src="../assets/js/jquery-1.10.2.js"></script>
			<!--  Core Bootstrap Script -->
			<script src="../assets/js/bootstrap.js"></script>
		</body>

		</html>
<?php
	} else
		echo '<script>alert("Vous n\'êtes pas autorisé à vous connecter sur cette page !");
    window.location.replace("/");
                    </script>';
} else
	echo '<script>alert("Veuillez vous identifier !");
    window.location.replace("/");
                    </script>';
?>