<?php
header('Content-type:text/html; charset=utf-8');
if (isset($_SESSION['id']))
{
    if(isset($_SESSION['statut'])==1)
    {
        ?>
<!DOCTYPE html>
<html lang="fr">

<head>
<meta charset="utf-8">
<title>Ajout de statut</title>

<meta name="Author" lang="fr" content="GAMARDE Sébastien & SAMSON Denis & PLAISIER Sylvain & DEPOUILLY Mickael"> 
<meta name="description" content="Appli Frais Algobreizh" />
<meta name="robots" content="noindex, nofollow, noarchive" />

 <link href="../assets/css/bootstrap.css" rel="stylesheet" />
    <!--  Font-Awesome Style -->
    <link href="../assets/css/font-awesome.min.css" rel="stylesheet" />
    <!--  Animation Style -->
    <!--  Google Font Style -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!--  Custom Style -->
    <link href="../assets/css/style.css" rel="stylesheet" />
</head>

<body>
		<?php include '../content/nav.php';?>
	 <div class="row pad-top-botm">
	 	 <div class="row text-center ">
                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                    <h2 data-wow-delay="0.3s" class="wow rollIn animated"><strong>Ajout d'un statut</strong></h2>
                    <p class="sub-head">Le statut sera ajoutée à la base de donnée.</p>
                    
                </div>
            </div>
          <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-8 col-xs-offset-2	">
			<form name="form-inscription" method="post" action="../admin/ajout_statut.php">
				<div class="form-group">
						<input placeholder="Nom du statut" class="form-control"
							name="statut" type="text" required />
				</div>
					<div class="form-group">
						<button type="submit" class="btn btn-success btn-block btn-lg wow rotateIn animated " data-wow-delay="0.8s" value="1" name="valid_statut" class="btn-blue">Créer le statut</button>
					</div>
			</form>
			</div>
			</div>
			 <script src="../assets/js/jquery-1.10.2.js"></script>
    		<!--  Core Bootstrap Script -->
   			 <script src="../assets/js/bootstrap.js"></script>
</body>
</html>
<?php
}
    else
    echo'<script>alert("Vous n\'êtes pas autorisé à vous connecter sur cette page !");
    window.location.replace("/algobreizh");
                    </script>';
}
else
echo'<script>alert("Veuillez vous identifier !");
    window.location.replace("/algobreizh");
                    </script>';
?>
