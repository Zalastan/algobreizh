<?php
header('Content-type:text/html; charset=utf-8');
if (isset($_SESSION['id']))
{
    if(isset($_SESSION['statut'])==1)
    {
        ?>
<!DOCTYPE html>
<html lang="fr">

<head>
<meta charset="utf-8">
<title>Inscription</title>
<meta name="Author" lang="fr" content="GAMARDE Sébastien & SAMSON Denis & PLAISIER Sylvain & DEPOUILLY Mickael"> 
<meta name="description" content="Appli Frais Algobreizh" />
<meta name="robots" content="noindex, nofollow, noarchive" />
<meta name="robots" content="noindex, nofollow, noarchive" />

 <link href="../assets/css/bootstrap.css" rel="stylesheet" />
    <!--  Font-Awesome Style -->
    <link href="../assets/css/font-awesome.min.css" rel="stylesheet" />
    <!--  Animation Style -->
    <!--  Google Font Style -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!--  Custom Style -->
    <link href="../assets/css/style.css" rel="stylesheet" />
</head>

<body>
		<?php include '../content/nav.php';?>
	 <div class="row pad-top-botm">
	 	 <div class="row text-center ">
                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                    <h2 data-wow-delay="0.3s" class="wow rollIn animated"><strong>Ajout d'un utilisateur</strong></h2>
                    <p class="sub-head">Le mot de passe sera généré automatiquement et envoyé par mail à l'utilisateur.</p>
                    
                </div>
            </div>
          <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-8 col-xs-offset-2	">
			<form name="form-inscription" method="post" action="../admin/inscription.php">
				<div class="row">
					<div class="form-group text-center">
						<label for="civilite">Civilité : </label> <select name="civilite"
							id="civilite">
							<option value="Monsieur">Monsieur</option>
							<option value="Madame">Madame</option>
						</select>
					</div>
				</div>
				
					<div class="form-group">
						<input placeholder="Nom d'utilisateur" class="form-control"
							name="username" type="text" required />
					</div>
				
				
					<div class="form-group">
						<input placeholder="Nom" name="nom" type="text"
							class="form-control" required />
					</div>
				
				
					<div class="form-group">
						<input placeholder="Prénom" name="prenom" type="text"
							class="form-control" required />
					</div>
				
				
					<div class="form-group text-center">
						<label for="statut">Statut : </label> <select name="statut"
							id="statut">
							<option value="administrateur">Administrateur</option>
							<option value="comptable">Comptable</option>
							<option value="commercial">Commercial</option>
							<option value="direction">Secrétaire</option>
						</select>
					</div>
				
				
					<div class="form-group">
						<input placeholder="Adresse 1" name="adresse1" id="adresse1"
							type="text" class="form-control" required />
					</div>
				
				
					<div class="form-group">
						<input placeholder="Adresse 2" name="adresse2" id="adresse2"
							type="text" class="form-control" />
					</div>
				
				
					<div class="form-group">
						<input id="codePostal" class="form-control" type="text"
							placeholder="Code postal" name="codePostal" required/>
					</div>
				
				
					<div class="form-group">
						<input id="ville" class="form-control" type="text"
							placeholder="Ville" name="ville" required />
					</div>
				
				
					<div class="form-group">
						<input id="pays" class="form-control" type="text"
							placeholder="Pays" name="pays" required />
					</div>
				
				
					<div class="form-group">
						<input id="telephoneFixe" class="form-control" type="tel"
							placeholder="Numéro de fixe" name="telephoneFixe"  />
					</div>
				
					<div class="form-group">
						<input id="telephonePortable" class="form-control" type="tel"
							placeholder="Numéro de portable" name="telephonePortable" required/>
					</div>
					<div class="form-group">
						<input id="email" class="form-control" type="email"
							placeholder="E-mail" name="email" required />
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-success btn-block btn-lg wow rotateIn animated " data-wow-delay="0.8s" value="1" name="inscription" class="btn-blue">Créer le compte</button>
					</div>
			</form>
			</div>
			</div>
			 <script src="../assets/js/jquery-1.10.2.js"></script>
    		<!--  Core Bootstrap Script -->
   			 <script src="../assets/js/bootstrap.js"></script>
</body>
</html>
<?php
}
    else
    echo'<script>alert("Vous n\'êtes pas autorisé à vous connecter sur cette page !");
    window.location.replace("/algobreizh");
                    </script>';
}
else
echo'<script>alert("Veuillez vous identifier !");
    window.location.replace("/algobreizh");
                    </script>';
?>
