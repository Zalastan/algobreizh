<?php
header('Content-type:text/html; charset=utf-8');

if (isset($_SESSION['id'])) {

    if ((isset($_SESSION['statut']) && $_SESSION['statut'] == 4)) {
?>
        <!DOCTYPE html>
        <html lang="fr">

        <head>
            <meta charset="utf-8">
            <title>Visiteur</title>
            <meta name="Author" lang="fr" content="GAMARDE Sébastien & SAMSON Denis & PLAISIER Sylvain">
            <meta name="description" content="Appli Frais Algobreizh" />
            <meta name="robots" content="noindex, nofollow, noarchive" />

            <link href="../assets/css/bootstrap.css" rel="stylesheet" />
            <!--  Font-Awesome Style -->
            <link href="../assets/css/font-awesome.min.css" rel="stylesheet" />
            <!--  Animation Style -->
            <!--  Google Font Style -->
            <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
            <!--  Custom Style -->
            <link href="../assets/css/style.css" rel="stylesheet" />
        </head>

        <body>
            <?php include '../content/nav.php'; ?>
            <div id="admin" class="pad-top-botm">
                <div class="container">
                    <div class="row text-center ">
                        <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                            <h2 data-wow-delay="0.3s" class="wow rollIn animated"><strong>Direction </strong></h2>
                            <p class="sub-head">Gérer vos frais en toute sérénité.</p>

                        </div>
                    </div>
                    <div class="row ">
                        <p>Rien dans cet espace, il faut le coder, et je ne me rappelle plus des fonctionnalités convenues. A toi de jouer !</p>
                    </div>


                </div>
            </div>
            <script src="../assets/js/jquery-1.10.2.js"></script>
            <!--  Core Bootstrap Script -->
            <script src="../assets/js/bootstrap.js"></script>
        </body>

        </html>
<?php
    } else {
        echo '<script>alert("Vous n\'êtes pas autorisé à vous connecter sur cette page !"); window.location.replace("/"); </script>';
    }
} else {
    echo '<script>alert("Veuillez vous identifier !");window.location.replace("/");</script>';
}
?>