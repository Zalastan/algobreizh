<?php
header('Content-type:text/html; charset=utf-8');
if (isset($_SESSION['id']))
{
    if(isset($_SESSION['statut'])==3 || isset($_SESSION['statut'])==4)
    {
?>

<!DOCTYPE html>
<html lang="fr">

<head>
<meta charset="utf-8">
<title>Validation des frais</title>
<meta name="Author" lang="fr" content="GAMARDE Sébastien & SAMSON Denis & PLAISIER Sylvain"> 
<meta name="description" content="Appli Frais Algobreizh" />
<meta name="robots" content="noindex, nofollow, noarchive" />
<meta name="robots" content="noindex, nofollow, noarchive" />

 	<link href="../assets/css/bootstrap.css" rel="stylesheet" />
    <!--  Font-Awesome Style -->
    <link href="../assets/css/font-awesome.min.css" rel="stylesheet" />
    <!--  Animation Style -->
    <!--  Google Font Style -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!--  Custom Style -->
    <link href="../assets/css/style.css" rel="stylesheet" />
    <link href="../assets/css/algobreizh.css" rel="stylesheet" />

	<!-- select search --> 
	<link href="../assets/css/jquery-ui/jquery-ui.css" rel="stylesheet">
	<link rel="stylesheet" href="../assets/js/chosen/chosen.css">
   
</head>

<body>
<?php include '../content/nav.php';?>
     <div class="row pad-top-botm">
             <div class="row text-center ">
                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                    <h2 data-wow-delay="0.3s" class="wow rollIn animated"><strong>Validation des frais</strong></h2>
                    <p class="sub-head">Valider selon les modalités, les remboursements.</p>
                    
                </div>
            </div>
            <div class="row text-center">
		    	<form id="choix">
		        	<div id="filtres" class="col-md-8 col-md-offset-2 col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 text-center vertical-align">
		      			<div class="side-by-side clearfix col-lg-4 col-md-4 col-sm-4 col-xs-4">
							<div>
								<label for="liste_utilisateurs">Utilisateurs :</label>
          						<select id="liste_utilisateurs" name="id_utilisateur" data-placeholder="" class="chosen-select">
			                        <option value='-1'></option>
			                        <?php
			                        	while ($donnes = $liste_utilisateurs->fetch()){
			                            	echo'<option value="'.$donnes['idUtilisateurs'].'">'.$donnes['nom'].' '.$donnes['prenom'].'</option>';
			                        	}
			                        ?>
          						</select>
       						</div>
      					</div>        
							<div class=" col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
				            		<label for="periode">Période :</label>
		          					<select id="periode" name='periode' class="chosen-select">
					          			<option value='-1'>
						                </option>
		          					</select>
		          					<img id="loader1" src="../assets/img/loader1.gif" alt="" style="display:none"/>
			    				</div>

						         <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
									<button id="btnEditFrais" type="button" style="display:none" class="btn btn-default" aria-label="Left Align">
									  <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
									</button>	
									<button id="btnSaveFrais" type="button" style="display:none" class="btn btn-default" aria-label="Left Align">
									  <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
									</button>
								</div></div></form></div><div class="row text-center"><div class="col-md-8 col-md-offset-2 col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 text-center"><form id="fraisEdit" name="frais" method="post" action="../comptable/validation_frais.php">
				<table id="liste_frais" class="table table-hover">	
	                    <!--    Afficher les frais de la période correspondante à la liste periode au dessus> X -->
	            </table>
     		</form></div></div>
	</div>
    <?php include_once ("../content/include.html");?>
   <script src="/views/comptable/consultation_frais.js"></script>
     <script type="text/javascript">
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
  </script>
</body>	
</html>
<?php
}
    else
    echo'<script>alert("Vous n\'êtes pas autorisé à vous connecter sur cette page !");
    window.location.replace("/");
                    </script>';
}
else
echo'<script>alert("Veuillez vous identifier !");
    window.location.replace("/");
                    </script>';
?>

