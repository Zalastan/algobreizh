<?php
header('Content-type:text/html; charset=utf-8');
if (isset($_SESSION['id']))
{
    if(isset($_SESSION['statut'])==2 || (isset($_SESSION['statut'])==4))
    {
        ?>
<!DOCTYPE html>
<html lang="fr">

<head>
<meta charset="utf-8">
<title>Gestion des frais</title>
<meta name="Author" lang="fr" content="GAMARDE Sébastien & SAMSON Denis & PLAISIER Sylvain"> 
<meta name="description" content="Appli Frais Algobreizh" />
<meta name="robots" content="noindex, nofollow, noarchive" />
 <link href="../assets/css/bootstrap.css" rel="stylesheet" />
    <!--  Font-Awesome Style -->
    <link href="../assets/css/font-awesome.min.css" rel="stylesheet" />
    <!--  Animation Style -->
    <!--  Google Font Style -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!--  Custom Style -->
    <link href="../assets/css/style.css" rel="stylesheet" />
    <!--  JQuery UI -->
    <link href="../assets/css/jquery-ui/jquery-ui.css" rel="stylesheet">
	<link href="../assets/css/jquery-ui/jquery-ui.theme.css" rel="stylesheet">
	<link href="../assets/css/jquery-ui/jquery-ui.structure.css" rel="stylesheet">
    
</head>

<body>
<?php include '../content/nav.php';?>
     <div class="row pad-top-botm">
             <div class="text-center ">
                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                    <h2 data-wow-delay="0.3s" class="wow rollIn animated"><strong>Gestion des frais</strong></h2>
                    <p class="sub-head">Mettez à jour vos frais sur votre plateforme en toute sérénité.</p>
                    
                </div>
            </div>
                <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-8 col-xs-offset-2 text-center">
                		<div class="alert alert-danger" id="msgErreur" role="alert"  style="display:none;">
                			<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
  							<span class="sr-only">Error:</span>
                		</div>
                		<div class="alert alert-success" id="msgOk" role="alert" style="display:none;">
                		    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
  							Vos frais ont bien été enregistrés.
                		</div>
                		
                        <div class="form-group">
                        <h4>Période : </h4>
                        <input placeholder="<?php echo $periode;?>" class="form-control"
                            name="periode" type="text" required disabled="disabled" />
                        </div>
                </div>
            <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-8 col-xs-offset-2 text-center">
                <form method="post" enctype="multipart/form-data">
                <h4>Type de frais : </h4>
                <select id="type_frais" name="type_frais">
                <?php
                        while ($donnees = $liste_nom_type_frais->fetch())
                            {
                            echo'<option value="'.$donnees['nom'].'">'.$donnees['nom'].'</option>';
                            }
                ?>
                </select>
                <div class="form-group">
                <h4>Libellé : </h4>
                <input title="Vous devez renseigner un libellé valide !" placeholder="Exemple, nuitée, repas, frais kilométrique" class="form-control"
                            name="libelle_frais" type="text" pattern="^[0-9-a-zA-Zàâäéèêëïîôöùûüç ]+$"/>
                    </div>
                
                    <div class="form-group">
                        <h4>Montant :</h4>
                        <input title="Vous devez renseigner un montant valide !" value="0" class="form-control"
                            name="montant_forfait" step="0.01" type="number" min="0" pattern="[0-9]"/>
                    </div>
                    
                    <div class="form-group">
                        <h4>Date :</h4>
                        <input title="Vous devez renseigner une date valide !" id="datepicker" placeholder="Exemple : 10-01-2015" class="form-control"
                            name="date_frais" type="text" 
                            pattern="(((0[1-9]|[12][0-9]|3[01])([-])(0[13578]|10|12)([-])(\d{4}))|(([0][1-9]|[12][0-9]|30)([-])(0[469]|11)([-])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([-])(02)([-])(\d{4}))|((29)(\.|-|\/)(02)([-])([02468][048]00))|((29)([-])(02)([-])([13579][26]00))|((29)([-])(02)([-])([0-9][0-9][0][48]))|((29)([-])(02)([-])([0-9][0-9][2468][048]))|((29)([-])(02)([-])([0-9][0-9][13579][26])))"
                        />
                    </div>
                    <div class="form-group">
                        <h4>Justificatif :</h4>
                    <input type="file" name="fileToUpload" id="fileToUpload"/>
                    </div>

                <div class="form-group">
                <button type="submit" value="1" name="envoi_frais" class="btn-success col-lg-6  col-md-6 col-sm-6 col-xs-6 btn-block btn-lg wow rotateIn animated" data-wow-delay="0.8s">Téléverser les frais.</button>
                </div>
            </form>
           
            </div>
    </div>

    <!--  Core Bootstrap Script -->

                
</body>
	
</html>
<?php
include_once ("../content/include.html");
}
    else
    echo'<script>alert("Vous n\'êtes pas autorisé à vous connecter sur cette page !");
    window.location.replace("/algobreizh");
                    </script>';
}
else
echo'<script>alert("Veuillez vous identifier !");
    window.location.replace("/algobreizh");
                    </script>';
?>

