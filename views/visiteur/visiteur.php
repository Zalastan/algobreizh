<?php
header('Content-type:text/html; charset=utf-8');
if (isset($_SESSION['id'])) {
    if ((isset($_SESSION['statut']) == 2) || (isset($_SESSION['statut']) == 4)) {
?>
        <!DOCTYPE html>
        <html lang="fr">

        <head>
            <meta charset="utf-8">
            <title>Visiteur</title>
            <meta name="Author" lang="fr" content="GAMARDE Sébastien & SAMSON Denis & PLAISIER Sylvain">
            <meta name="description" content="Appli Frais Algobreizh" />
            <meta name="robots" content="noindex, nofollow, noarchive" />

            <link href="../assets/css/bootstrap.css" rel="stylesheet" />
            <!--  Font-Awesome Style -->
            <link href="../assets/css/font-awesome.min.css" rel="stylesheet" />
            <!--  Animation Style -->
            <!--  Google Font Style -->
            <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
            <!--  Custom Style -->
            <link href="../assets/css/style.css" rel="stylesheet" />
        </head>

        <body>
            <?php include '../content/nav.php'; ?>
            <div id="admin" class="pad-top-botm">
                <div class="container">
                    <div class="row text-center ">
                        <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                            <h2 data-wow-delay="0.3s" class="wow rollIn animated"><strong>Gestion Commercial </strong></h2>
                            <p class="sub-head">Gérer vos frais en toute sérénité.</p>

                        </div>
                    </div>
                    <div class="row ">
                        <div class="col-lg-4 col-lg-offset-2 col-md-4  col-md-offset-2 col-sm-4 col-sm-offset-2  col-xs-6 ">

                            <div class="portfolio-item wow rotateIn animated" data-wow-delay="0.4s">


                                <img src="../assets/img/formulaire-contact.jpg" class="img-responsive " alt="" />
                                <div class="overlay">
                                    <p><a href="../visiteur/gestion_frais.php">Gérer mes frais</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">

                            <div class="portfolio-item wow rotateIn animated" data-wow-delay="0.4s">


                                <img src="../assets/img/stylo-comptes.jpg" class="img-responsive " alt="" />
                                <div class="overlay">
                                    <p><a href="../visiteur/consultation_frais.php">Consulter mes frais</a></p>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <script src="../assets/js/jquery-1.10.2.js"></script>
            <!--  Core Bootstrap Script -->
            <script src="../assets/js/bootstrap.js"></script>
        </body>

        </html>
<?php
    } else
        echo '<script>alert("Vous n\'êtes pas autorisé à vous connecter sur cette page !");
    window.location.replace("/");
                    </script>';
} else
    echo '<script>alert("Veuillez vous identifier !");
    window.location.replace("/");
                    </script>';
?>