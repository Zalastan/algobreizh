<?php
header('Content-type:text/html; charset=utf-8');
if (isset($_SESSION['id']))
{ ?>
	<!DOCTYPE html>
<html lang="fr">

<head>
<meta charset="utf-8">
<title>Profil</title>
<meta name="Author" lang="fr" content="GAMARDE Sébastien & SAMSON Denis & PLAISIER Sylvain"> 
<meta name="description" content="Appli Frais Algobreizh" />
<meta name="robots" content="noindex, nofollow, noarchive" />

 <link href="../assets/css/bootstrap.css" rel="stylesheet" />
    <!--  Font-Awesome Style -->
    <link href="../assets/css/font-awesome.min.css" rel="stylesheet" />
    <!--  Animation Style -->
    <!--  Google Font Style -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!--  Custom Style -->
    <link href="../assets/css/style.css" rel="stylesheet" />
</head>

<body>
	<?php include '../content/nav.php';?>
	 <div class="row pad-top-botm">
	 		 <div class="row text-center ">
                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                    <h2 data-wow-delay="0.3s" class="wow rollIn animated"><strong>Mon profil</strong></h2>
                    <p class="sub-head">Vous pouvez à tout moment modifier vos informations et votre mot de passe.</p>
                    
                </div>
            </div>
			<div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-8 col-xs-offset-2	">
				<form name="edition" method="post" action="../session/edition.php" >
				
					<div class="form-group">
						<input id="updateData_mail" class="form-control" type="email" placeholder="<?php if (isset($email)) echo $email; ?>" name="email" disabled="disabled"/>
					</div>
					<div class="form-group">
						<input id="updateData_username" class="form-control"  type="text" placeholder="<?php if (isset($_SESSION['username'])) echo $_SESSION['username']; ?>" name="username" disabled= "disabled"/>
					</div>
				
				
				
					<div class="form-group">
						<input id="updateData_lastName" class="form-control" type="text" placeholder="<?php if (isset($nom)) echo $nom; ?>" name="nom" disabled= "disabled"/>
					</div>
				
				
					<div class="form-group">
						<input id="updateData_name" class="form-control" type="text" placeholder="<?php if (isset($prenom)) echo $prenom; ?>" name="prenom" disabled= "disabled"/>
					</div>
				
				
					<div class="form-group">
						<input id="updateData_address" class="form-control" type="text" value ="<?php if (isset($adresse1)) echo $adresse1; ?>" placeholder="<?php if (isset($adresse1)) echo $adresse1; ?>" name="adresse1" required/>
					</div>
				
				
					<div class="form-group">
						<input id="updateData_address" class="form-control" type="text" value ="<?php if (isset($adresse2) && !empty($adresse2)) echo $adresse2; else echo 'Non renseigné'; ?>" placeholder="<?php if (isset($adresse2) && !empty($adresse2)) echo $adresse2; else echo 'Non renseigné'; ?>" name="adresse2" />
					</div>
				
				
					<div class="form-group">
						<input id="updateData_postCode" class="form-control" type="text" value="<?php if (isset($codePostal)) echo $codePostal; ?>" placeholder="<?php if (isset($codePostal)) echo $codePostal; ?>" name="codePostal" pattern="[0-9]{5}" required/>
					</div>
				
				
					<div class="form-group">
						<input id="updateData_city" class="form-control" type="text" value="<?php if (isset($ville)) echo $ville; ?>" placeholder="<?php if (isset($ville)) echo $ville; ?>" name="ville" required/>
					</div>
				
				
					<div class="form-group">
						<input id="updateData_country" class="form-control" type="text" value="<?php if (isset($pays)) echo $pays; ?>" placeholder="<?php if (isset($pays)) echo $pays; ?>" name="pays" required />
					</div>
				
				
					<div class="form-group">
						<input id="updateData_phone" class="form-control" type="tel" value="<?php if (isset($telephoneFixe) && $telephoneFixe>0) echo $telephoneFixe; else echo "Téléphone Fixe"; ?>" placeholder="<?php if (isset($telephoneFixe) && $telephoneFixe>0) echo $telephoneFixe; else echo "Téléphone Fixe"; ?>" name="telephoneFixe" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$"/>
					</div>
				
				
					<div class="form-group">
						<input id="updateData_smartphone" class="form-control" type="tel" value="<?php if (isset($telephonePortable) && $telephonePortable>0) echo $telephonePortable; else echo "Téléphone Portable"; ?>" placeholder="<?php if (isset($telephonePortable)) echo $telephonePortable; ?>" name="telephonePortable" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" required/>
					</div>
				
				
					<div class="form-group">
						<input id="updateData_job" class="form-control" type="text" placeholder="<?php if (isset($profession)) echo $profession; ?>" name="statut"  disabled="disabled"/>
					</div>
				
				
					<div class="form-group">
						<input id="updateData_date" class="form-control" type="date" placeholder="<?php if (isset($dateInscription)) echo $dateInscription; ?>"  name="dateInscription" disabled="disabled"/>
					</div>
					<div class="form-group">
					<button type="submit" name="edition" class="btn-success col-lg-6  col-md-6 col-sm-6 col-xs-6 btn-block btn-lg wow rotateIn animated" data-wow-delay="0.8s">Valider les modifications</button>
					<button type="button" class="btn-primary col-lg-6  col-md-6 col-sm-6 col-xs-6 btn-block btn-lg wow rotateIn animated" data-toggle="modal" data-target="#updatePassword">Modifier le mot de passe</button>
					</div>
				</form>
			</div>
		</div>

		<div class="modal fade" id="updatePassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h3 class="modal-title" id="myModalLabel">Modifier mon mot de passe</h3>
					</div>
				<div class="modal-body">
					<form role="form" name="editpassword" action="../session/edition.php" method="post" >
						<div class="form-group">
						<input id="paswword" class="form-control" type="password" placeholder="Mot de passe actuel"  name="password" required/>
				</div>
					<div class="form-group">
						<input id="password1" class="form-control" type="password" placeholder="Nouveau mot de passe*"  name="password1" 
						pattern="^((?=.*\d)(?=.*[a-zA-Z])[a-zA-Z0-9!@#$%&*]{6,20})$" required />
					</div>
					<div class="form-group">
						<label for="updateData_newPassword">
						*de 6 à 20 caractères <br/>
						- au moins une lettre minuscule <br/>
						- au moins une lettre majuscule <br/>
						- au moins un chiffre <br/>
						- peut contenir ces caractères spéciaux: !@#$%&*</label>
					</div>
				<hr />
				
					<div class="form-group">
						<input id="password2" class="form-control" type="password" placeholder="Veuillez retaper votre nouveau mot de passe*"  name="password2" pattern="^((?=.*\d)(?=.*[a-zA-Z])[a-zA-Z0-9!@#$%&*]{6,20})$" required/>
					</div>
			
					<button type="submit" name="editpassword" class="text-center btn-block btn-lg wow rotateIn animated btn-success">Valider</button>
				</form>
			</div>
		</div>
	</div>
</div>
 <script src="../assets/js/jquery-1.10.2.js"></script>
    <!--  Core Bootstrap Script -->
    <script src="../assets/js/bootstrap.js"></script>
</body>
</html>
<?php
}
else{
echo'<script>alert("Veuillez vous identifier !");
    window.location.replace("/algobreizh");
                    </script>';
}
?>