<?php
/* CONTROLLEUR DE LA PAGE INDEX des administrateur */
session_start();
header('Content-type:text/html; charset=utf-8');
if (isset($_SESSION['id'])) // on vérifie que l'utilisateur est bien logué
{
	if(isset($_SESSION['statut'])) // on vérifie qu'il est bien admin
	{
require_once '../views/admin/index.php'; 
// importation des données d'accès à la base de donnée
// génération de la vue
require_once'../models/connexion/connectdb.php';
	}
	else
	echo'<script>alert("Vous n\'êtes pas autorisé à vous connecter sur cette page !");
	window.location.replace("/");
					</script>';
}
else
echo'<script>alert("Veuillez vous identifier !");
	window.location.replace("/");
					</script>';
