<?php
session_start();
header('Content-type:text/html; charset=utf-8');
if (isset($_SESSION['id']))
{
	if(isset($_SESSION['statut'])==1)
	{
	
	

// importation des données d'accès à la base de donnée
require_once'../models/connexion/connectdb.php';
require_once '../models/admin/ajout_statut.php';

	if (isset ( $_POST ['statut'] )){
		$statut = htmlspecialchars ( $_POST ['statut'] );
		ajout_statut($statut);
		
		echo '<script>
				alert("Le statut à bien été ajouté !")
			  </script>';
		
	}
	require_once '../views/admin/ajout_statut.php';


	}
	else
	echo'<script>alert("Vous n\'êtes pas autorisé à vous connecter sur cette page !");
	window.location.replace("/algobreizh");
					</script>';
}
else
echo'<script>alert("Veuillez vous identifier !");
	window.location.replace("/algobreizh");
					</script>';
?>