<?php
session_start();
header('Content-type:text/html; charset=utf-8');
if (isset($_SESSION['id']))
{
	if(isset($_SESSION['statut'])==1)
	{
	
	

// importation des données d'accès à la base de donnée
require_once'../models/connexion/connectdb.php';
require_once '../models/admin/suppression.php';

	if (isset ( $_POST ['suppression'] )){
		$idUtilisateurs = htmlspecialchars ( $_POST ['suppression'] );
		suppressionUtilisateur($idUtilisateurs);
		
		echo '<script>
				alert("Le membre à bien été supprimé !")

			  </script>';
		
	}
	require_once '../views/admin/suppression.php';


	}
	else
	echo'<script>alert("Vous n\'êtes pas autorisé à vous connecter sur cette page !");
	window.location.replace("/");
					</script>';
}
else
echo'<script>alert("Veuillez vous identifier !");
	window.location.replace("/");
					</script>';
?>